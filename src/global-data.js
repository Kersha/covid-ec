export let globalData = {
    cases: {
        labels: [
            'confirmados',
            'fallecidos',
            'descartados',
            'recuperados',
        ],
        datos: [
            35306, 2939, 42392, 3536
        ]
    },
    infectedState: {
        labels: [
            'recuperados',
            'alta hospitalaria',
            'alta epidemiologica',
            'aislamiento domiciliario',
            'hospitalizados estable',
            'hospitalizados reservado',
            'fallecidos'
        ],
        datos: [
            3536, 3638,10087,14486, 415,205,2939
        ]
    },
    total: 35306
}
// se dejo de publicar 5137 preubas a partir del 6 de abril